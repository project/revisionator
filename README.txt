CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

The Revisionator module clean up insightly old revisions! Revisionator removes 
revisions older than the specified age, leaving a minimum number of revisions
per node.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/revisionator

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/search/revisionator


REQUIREMENTS
------------

No special requirements.


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module.
   See: https://www.drupal.org/node/895232 for further information.


CONFIGURATION
-------------

 * Configure the module in Administration » Configuration »
   Content authoring » Revisionator:

   - You can set the module settings and optionally start a dry run. For this 
     you need the 'Administer Revisionator' permission.

 * The revisions will be cleaned once cron runs.

 
MAINTAINERS
-----------

Current maintainers:
 * Adrian Cid Almaguer (adriancid) - https://www.drupal.org/u/adriancid
 * Mark Styles (lambic) https://www.drupal.org/u/lambic


This project has been sponsored by:

 * Drupiter
 * Ville de Montréal
 * McGill University
