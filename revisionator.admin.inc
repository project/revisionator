<?php

/**
 * @file
 * Admin settings form.
 */

/**
 * Page callback: Form constructor for the module administration form.
 */
function revisionator_admin() {
  $form['revisionator_age'] = array(
    '#type' => 'textfield',
    '#title' => t('Delete revisions older than'),
    '#default_value' => variable_get('revisionator_age', 365),
    '#description' => t('Specify the number of days to keep revisions for.'),
  );
  $form['revisionator_min'] = array(
    '#type' => 'textfield',
    '#title' => t('Minimum revisions to keep per node'),
    '#default_value' => variable_get('revisionator_min', 50),
    '#description' => t('Specify the minimum number of revisions to keep for each node. This will override the age setting above. For example of you set this to 10, then 10 revisions will always be kept even if they are older than the age above.'),
  );
  $form['revisionator_cron'] = array(
    '#type' => 'textfield',
    '#title' => t('Revisions to delete per cron run'),
    '#default_value' => variable_get('revisionator_cron', 50),
    '#description' => t('Revisions get deleted whenever cron runs. To avoid overloading cron this setting will control how many revisions we delete each time it runs.'),
  );
  $form['dry_run'] = array(
    '#type' => 'checkbox',
    '#title' => t('Perform a dry run'),
    '#default_value' => 0,
  );
  $form['#submit'][] = 'revisionator_admin_submit';
  return system_settings_form($form);
}

/**
 * Submission handler.
 */
function revisionator_admin_submit($form, &$form_state) {
}

/**
 * Admin settings form validation handler.
 */
function revisionator_admin_validate($form, &$form_state) {
  if (!is_numeric($form_state['values']['revisionator_age'])) {
    form_set_error('revisionator_age', t('Age must be a number of days.'));
  }
  if (!is_numeric($form_state['values']['revisionator_min'])) {
    form_set_error('revisionator_min', t('Minimum must be a number.'));
  }
  if (!is_numeric($form_state['values']['revisionator_cron'])) {
    form_set_error('revisionator_cron', t('Deletes per cron must be a number.'));
  }
  if ($form_state['values']['dry_run']) {
    revisionator_delete($form_state['values']['revisionator_cron'], TRUE);
  }
  unset($form_state['values']['dry_run']);
}

/**
 * Function to do the deletion work.
 */
function revisionator_delete($count = 0, $dry_run = FALSE) {
  $age = time() - (variable_get('revisionator_age', 365) * 24 * 60 * 60);
  $min = variable_get('revisionator_min', 50);

  $q = db_select('node_revision', 'nr')
    ->fields('nr', array('nid', 'title'))
    ->groupBy('nr.nid')
    ->having('COUNT(*) > :min', array(':min' => $min));

  $q->addExpression('COUNT(*)', 'nid_count');
  $nodes = $q->execute();

  foreach ($nodes as $node) {
    $revs = db_select('node_revision', 'nr')
      ->fields('nr', array('vid'))
      ->condition('nr.nid', $node->nid)
      ->condition('nr.timestamp', $age, '<')
      ->range($min, $count ? $count : 10000)
      ->execute();
    foreach ($revs as $rev) {
      if ($dry_run) {
        drupal_set_message(t(
          "Revision %r on node %n (%t) would be deleted",
          array('%r' => $rev->vid, '%n' => $node->nid, '%t' => $node->title)));
      }
      else {
        node_revision_delete($rev->vid);
      }
    }
  }
}
